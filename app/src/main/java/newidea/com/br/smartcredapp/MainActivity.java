package newidea.com.br.smartcredapp;

import android.media.Image;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupComponentes();
    }

    private void setupComponentes() {
        EditText edtCnpj = (EditText) findViewById(R.id.edt_cnpj);
        edtCnpj.addTextChangedListener(MaskUtil.insert(edtCnpj, MaskType.CNPJ));

        Button button = (Button) findViewById(R.id.btn_consulta);

        edtCnpj.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                ImageView image = (ImageView) findViewById(R.id.imageSituacaoCNPJ);
                image.setImageResource(android.R.color.transparent);

                TextView textView = (TextView) findViewById(R.id.txt_resultado);
                textView.setText("");
            }
        });

        // Capture button clicks
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                EditText edtCnpj = (EditText) findViewById(R.id.edt_cnpj);
                final String url = "http://shielded-brushlands-81401.herokuapp.com/indicadorDashPagtoEmDia?cnpj=" + edtCnpj.getText().toString();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        new ReadJSON().execute(url);
                    }
                });
            }
        });
    }


    class ReadJSON extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();

                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();
                String line = "";

                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)

                }

                return buffer.toString();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }


        @Override
        protected void onPostExecute(String content) {
            DashIndicadorTO to = new DashIndicadorTO();
            to.setLog(new ArrayList<LinhaDashIndicadorTO>());

            try {
                JSONObject jsonObject = new JSONObject(content);

                to.setNomeEmpresa(jsonObject.getString("nomeEmpresa"));
                to.setDescricaoIndicador(jsonObject.getString("descricaoIndicador"));
                to.setPercentualIndicador(jsonObject.getString("percentualIndicador"));
                double percentual = Double.parseDouble(to.getPercentualIndicador());

                ImageView image = (ImageView) findViewById(R.id.imageSituacaoCNPJ);
                TextView textView = (TextView) findViewById(R.id.txt_resultado);
                String imgUrl = "";
                if (percentual >= 70.00) {
                    imgUrl = "http://www.ejobsnepal.com/wp-content/uploads/2015/11/How-to-know-if-Jobs-is-real-or-not.png";
                    textView.setText("Aprovado");
                } else {
                    if (percentual >= 40 && percentual < 70) {
                        imgUrl = "http://www.clker.com/cliparts/5/6/f/3/11971252291061148562zeimusu_Warning_notification.svg.med.png";
                        textView.setText("Atenção");
                    } else {
                        imgUrl = "https://s-media-cache-ak0.pinimg.com/originals/d8/3e/2f/d83e2f9ffe32295147baba26b58317d8.jpg";
                        textView.setText("Reprovado");
                    }
                }

                Picasso.with(getApplicationContext()).load(imgUrl).into(image);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }



}
