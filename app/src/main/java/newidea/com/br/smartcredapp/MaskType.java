package newidea.com.br.smartcredapp;

/**
 * Created by fabio on 07/05/17.
 */

public enum MaskType {
    CPF,
    CNPJ
}
